import * as d3 from "d3"
import {legendColor} from 'd3-svg-legend'
import * as topojson from 'topojson-client'
import topojsonFile from './topojsonFile.json'
import './App.css';
import * as React from 'react'
import Select from 'react-select'

function App() {
    const [metric, setMetric] = React.useState({value: 'cases', label: 'Cases'});
    const [map, setMap] = React.useState({metric: metric.value});

    const options = [
        {value: 'cases', label: 'Cases'},
        {value: 'deaths', label: 'Deaths'},
        {value: 'todayCases', label: 'Today cases'},
        {value: 'todayDeaths', label: 'Today deaths'},
        {value: 'recovered', label: 'Recovered'},
        {value: 'critical', label: 'critical'},

    ]

    const drawMap = () => {

        // Define size related letiables
        // const dimension = d3.select(".visualisation")
        //     .node()
        //     .parentNode
        //     .getBoundingClientRect();
        const margin = 90;
        const width = 900;
        const height = 500;
        const aspect = width / (height - margin);
        const rotate = -9.9;

        const zoom = d3.zoom()
            .scaleExtent([1, 30])
            .translateExtent([[0, 0], [width, height]])
            .on('zoom', function (event) {
                d3.select('g').attr('transform', event.transform)
            });

        // Add the core svg block
        const svg = d3.select(".visualisation")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", `0 0 ${width} ${height}`)
            .call(zoom);

        const globe = svg.append("g");

        const tooltip = d3.select('.visualisation').append('div')
            .attr('class', 'hidden tooltip');

        const projection = d3.geoMercator()
            .rotate([rotate, 0])
            .scale(height / (1.4 * Math.PI))
            .translate([width / 2, (height - margin) / 1.2]);

        const geoPath = d3.geoPath()
            .projection(projection);

        const colorScale = d3.scaleSqrt(["#ddd", "#777", "#000"]);
        selectData(map);
        colorScale.domain([0,
            d3.median(map.features, d => d.properties.dataPoint),
            d3.max(map.features, d => d.properties.dataPoint)]);
        draw(globe, map, geoPath, colorScale, tooltip);
        drawLegend(svg, height, margin, colorScale);
    };

    function draw(globe, map, geoPath, colorScale, tooltip) {
        globe.selectAll("path.country").remove();
        globe.selectAll("path.country")
            .data(map.features)
            .enter()
            .append("path")
            .attr("class", "country")
            .attr('d', geoPath)
            .style("fill", d => colorScale(d.properties.dataPoint))
            .on('mousemove', function (event, d) {
                tooltip.classed('hidden', false)
                    .html("<h6>" + d.properties.country + ": " + d.properties.dataPoint + "</h6>")
                    .attr('style', 'left:' + (event.pageX + 15) + 'px; top:' + (event.pageY + 20) + 'px');
            })
            .on('mouseout', function () {
                tooltip.classed('hidden', true);
            });
    };

    function drawLegend(svg, height, margin, colorScale) {
        svg.select(".legendLinear").remove();
        svg.append("g")
            .attr("class", "legendLinear")
            .attr("transform", "translate(10," + (height - margin) + ")");

        let shapeWidth = 40,
            cellCount = 5,
            shapePadding = 2
        //legendTitle = map.metric.replace("PerOneMillion", "")  + " per million population: ";

        let legendLinear = legendColor()
            .shape("rect")
            .shapeWidth(shapeWidth)
            .cells(cellCount)
            .labelFormat(d3.format(".3s"))
            .orient('horizontal')
            .shapePadding(shapePadding)
            .scale(colorScale);

        svg.select(".legendLinear")
            .append("rect")
            .attr("class", "legendBackground")
            .attr("x", -5)
            .attr("y", -22)
            .attr("opacity", 0.9)
            .attr("rx", 8)
            .attr("ry", 8)
            .attr("height", margin);

        svg.select(".legendLinear")
            .call(legendLinear);
    };

    function selectData(map) {
        map.features.forEach((d) => {
            var entry1 = map.data.filter(t => t.countryInfo.iso3 == d.properties.ISO_A3)[0];
            if (entry1) {
                d.properties.dataPoint = entry1[map.metric];
                d.properties.country = entry1.country;
            } else {
                d.properties.dataPoint = 0;
                d.properties.country = "Unknown";
            }
        })
    };

    React.useEffect(() => {
        map.features && map.data && drawMap();
    }, [map.features])

    React.useEffect(() => {
        map.features && selectData(map);
    }, [map.metric])

    React.useEffect(() => {
        Promise.all([
            d3.json('https://disease.sh/v3/covid-19/countries?yesterday=true'),

        ]).then(function ([data]) {
            let countries = topojson.feature(topojsonFile, "world");
            setMap({...map, features: countries.features.filter((d) => d.properties.ISO_A3 !== "ATA"), data: data})
        })
    }, [])

    React.useEffect(() => {
        map.features && setMap({...map, metric: metric.value});
    }, [metric])

    return (
        <div>
            <Select options={options} onChange={setMetric} defaultValue={options[0]}></Select>
            <div className="visualisation">
            </div>
        </div>
    );
}

export default App;
